MANPREFIX=/usr/local/share/man
MANPAGE=threadripper
SECTION=7

install:
	mkdir -p $(MANPREFIX)/man$(SECTION)
	cp $(MANPAGE).$(SECTION) $(MANPREFIX)/man$(SECTION)/$(MANPAGE).$(SECTION)
	chmod 644 $(MANPREFIX)/man$(SECTION)/$(MANPAGE).$(SECTION)
