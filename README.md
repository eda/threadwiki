# PLASMA Server man-page 

Original source, downloaded from teams sharepoint in `threadripper-wiki.mht`,
converted to unix man (7) page.

Preview with:

```sh
$ groff -Tascii -man threadripper.7 | less
```

## Service

Under `service/` is a systemd service that will pull and install the man page
periodically.
