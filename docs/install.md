# AlmaLinux Server Setup Guide

## Backup User Data

Before starting, back up existing user data to ensure nothing is lost during
installation.

1. **Create Backup Location**: Ensure the backup drive is mounted under
   `/mnt/data/backup`.
    - Check with `lsblk` and `mount` commands.
    
1. **Use `rsync`** to copy user home directories with permissions preserved:
   ```
   # rsync -avP /home/ /mnt/data/backup/
   ```
    - `-a`: Archive mode to preserve symbolic links, permissions, timestamps, etc.
    - `-v`: Verbose to show the files being copied.
    - `-P`: Show Progress
    - `/home/`: Source directory.
    - `/mnt/data/backup/`: Destination directory on the backup drive.

## Install AlmaLinux

### Base System Installation

1. **Download AlmaLinux ISO**: 
    - Download the latest AlmaLinux ISO from [https://almalinux.org](https://almalinux.org).
1. **Create Installation Media**: 
    - Use `dd` to create a bootable USB drive from the ISO:
      ```
      # dd if=/path/to/almalinux.iso of=/dev/sdX bs=4M status=progress
      ```
    - Replace `/dev/sdX` with the device of your USB drive (check using `lsblk`).
    - Do not write to partition, write to whole disk
1. **Reboot and Install**:
    - Plug in the USB and reboot the system, then boot from the USB drive.
    - Choose **Minimal Installation** without a desktop environment.
1. **Partition Disk**:
    - When prompted, select **custom partitioning** and create:
        + `/` (root) partition using the remaining space on the disk.
        + Optionally, a `swap` partition if not using a swap file.
1. **Configure Static IP Address**:
    - Retrieve network informatin with `nmcli`:
      ```
      $ nmcli dev show enp68s0
      ```
    - During the network configuration, assign the IP address `134.103.16.14`:
        + IP: `134.103.16.14/25`
        + Netmask: `255.255.255.128`
        + Broadcast: `134.103.16.127`
        + Gateway: `134.103.16.1`
        + DNS: Use your preferred DNS: `134.103.76.12`, `134.103.76.13`

## Post-Installation Configuration

### Mounting Drives

1. **Identify Drives**:
    - Use `lsblk` to identify the external drive partitions `/dev/sdb1` and
      `/dev/sdc1`.
1. **Edit `/etc/fstab`** to automatically mount the drives at boot:
   ```
   /dev/sdb1  /mnt/data  ext4  defaults,auto,nofail,noatime,rw  0  2
   /dev/sdc1  /mnt/tools ext4  defaults,auto,nofail,noatime,rw  0  2
   ```
    - Make sure both partitions are formatted as `ext4` (or another supported filesystem).
    - Use `mount -a` to verify the entries are correct.
    - `nofail` is important, ensuring the system boots even if the disk is
      unavailable.

### Creating User Accounts

1. **Install `sudo`**:
   ```
   # dnf install sudo -y
   ```
1. **Create User Accounts**:
   ```
   # useradd -m -s /bin/bash <username>
   # passwd <username>
   ```
1. **Add Users to the Wheel Group** (for sudo privileges):
   ```
   $ usermod -aG wheel <username>
   ```

## Installing Required Software

### Miscellaneous Tools

1. Install basic utilities:
   ```
   # dnf install git curl wget ksh zsh tmux neovim htop btop bat nvtop zoxide \
       lf lsd neofetch cpufetch ripgrep fd-find the_silver_searcher fzf fzy csvtk \
       jq chafa sl gcc make automake autoconf libtool xorg-x11-server-devel \
       imagemagick libX11-devel libXext-devel libXt-devel libXft-devel xterm -y
   ```
1. To install an older version of `gcc`:
    - Enable the SCL repository:
      ```
      # dnf install centos-release-scl
      ```
    - Install desired version of GCC (for example, GCC 7):
      ```
      # dnf install devtoolset-7
      ```
    - Enable the installed toolset:
      ```
      $ scl enable devtoolset-7 bash
      ```

### NVIDIA Drivers

1. **Disable `nouveau` (open-source NVIDIA driver)**:
    - Create a `modprobe` blacklist file:
      ```
      # echo 'blacklist nouveau' > /etc/modprobe.d/blacklist-nouveau.conf
      ```
    - Regenerate the initramfs:
      ```
      # dracut --force
      ```
1. **Install NVIDIA Drivers**:
    - Enable the `EPEL` and `RPM Fusion` repositories:
      ```
      # dnf install epel-release -y
      # dnf install https://download1.rpmfusion.org/free/el/rpmfusion-free-release-9.noarch.rpm -y
      # dnf install https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-9.noarch.rpm -y
      ```
    - Install the NVIDIA drivers:
      ```
      # dnf install akmod-nvidia xorg-x11-drv-nvidia-cuda -y
      ```
1. **Test Installation**:
    - Install `glxgears` to test the drivers:
      ```
      # dnf install mesa-demos -y
      $ glxgears
      ```

### Window Mangement

#### WM vs DE

Desktop Environments (DEs) provide a Window Manager (WM) and a set
applications, such as terminal emulators, file managers, office applications,
etc. Regardless, an Xorg-based utility should be used, instead of Wayland.

**Comprehensive lists**:

- [Window Managers](https://wiki.archlinux.org/title/Window_manager)
- [Desktop Environments](https://wiki.archlinux.org/title/Desktop_environment)

1. **Install chosen DE or WM**:
   ```
   # dnf install <DE or WM>
   ```
1. **Configure locally**:
    - Under `~/Xclients` and/or `~/.xinitrc`:
      ```
      exec <DE or WM>
      ```
    - If not configured `xrdp` will use what ever it finds under
      `/etc/X11/Xclients`

#### X Forwarding

Regardless of DE, **enable X forwarding over SSH**:

- Edit the `/etc/ssh/sshd_config` file:
  ```
  X11Forwarding yes
  X11DisplayOffset 10
  ```
- Restart the SSH service:
  ```
  # systemctl restart sshd
  ```

### Remote Desktop

#### XRDP

1. **Install xrdp**:
   ```
   # dnf install xrdp -y
   # systemctl enable xrdp
   # systemctl start xrdp
   ```
1. **Configure PAM for xrdp**:
   Ensure that the `/etc/pam.d/xrdp-sesman` file is set up to use PAM for
   authentication:
   ```
   auth       required    password-auth
   account    required    password-auth
   ```
1. **Firewall Configuration**:
   ```
   # firewall-cmd --permanent --add-port=3389/tcp
   # firewall-cmd --reload
   ```
    - Allow RDP through the firewall
1. **Global configuration**:
    - Create or modify `/etc/xrdp/startwm.sh`
      ```sh
      #!/bin/sh
      if [ -r "$HOME/.xinitrc" ]; then
          exec "$HOME/.xinitrc"
      else
          exec twm
      fi
      ```
1. **Allow non-root users to connect**:
   Add the following lines to `/etc/X11/Xwrapper.config`:
   ```
   allowed_users=anybody
   needs_root_rights=no
   ```
1. **Connect Using an RDP Client**:
    - Connect using any RDP client by entering the server's IP address and Linux
      Login/Password.
1. **If there is an error** stating something along the lines of
    - `error loading libvnc.so specified in xrdp.ini. please add a valid entry
      like lib=libxrdp-vnc.so`
    - See this [github issue](https://github.com/neutrinolabs/xrdp/issues/2690)
    - Remove the current install of `xrdp` and install the development version:
      ```
      # dnf remove xrdp
      # dnf install https://kojipkgs.fedoraproject.org//packages/xrdp/0.9.22/4.el9/x86_64/xrdp-0.9.22-4.el9.x86_64.rpm
      ```

#### VNC

1. **Server side Installation**:
    - Install the `tigerVNC` server
      ```
      # dnf install tigervnc-server
      ```
1. **Create a Global `xstartup` Script**:
    - Create the file `/etc/vnc/xstartup` with the following content:
      ```bash
      #!/bin/bash
      exec <DE or WM>
      ```
    - Make it executable:
      ```
      # chmod +x /etc/vnc/xstartup
      ```
1. **Create a Global VNC Configuration File**:
    - Place a global VNC configuration file in `/etc/vnc/config`:
      ```
      session=<DE or WM>
      geometry=1920x1080
      localhost
      securitytypes=tlsvnc
      ```
    - This file will act as a default configuration for all users.
1.**Create `systemd` Service with Global Configuration**:
    - Create the `/etc/systemd/system/vncserver@.service` service file to use
      both the global `xstartup` and global `config`:
      ```ini
      [Unit]
      Description=Remote desktop service (VNC) for %i
      After=syslog.target network.target

      [Service]
      Type=forking
      User=%i
      PAMName=login
      PIDFile=/home/%i/.vnc/%H:%i.pid
      ExecStartPre=-/usr/bin/vncserver -kill :%i > /dev/null 2>&1
      ExecStart=/usr/bin/vncserver :%i -localhost no -geometry 1920x1080 -xstartup /etc/vnc/xstartup -config /etc/vnc/config
      ExecStop=/usr/bin/vncserver -kill :%i

      [Install]
      WantedBy=multi-user.target
      ```
    - The `-xstartup /etc/vnc/xstartup` ensures all users use the global
      `xstartup` script.
    - The `-config /etc/vnc/config` ensures all users share the global VNC
      configuration file.
    - Each user will still get a unique display number dynamically based on
      their user ID (`:1`, `:2`, etc.).
1. **Enable and Start `tigerVNC` Service for All Users**:
    - Run the following command or put it in a script and execute it (requires
      `root` or `sudo`):
      ```bash
      #!/bin/bash
      for user in $(awk -F: '$3 >= 1000 && $1 != "nobody" {print $1}' /etc/passwd); do
         systemctl enable vncserver@$user.service
         systemctl start vncserver@$user.service
      done
      ```
    - `awk -F: '$3 >= 1000 && $1 != "nobody"`: This filters out system users
      (those with a UID less than 1000) and excludes the `nobody` user.
    - `systemctl enable vncserver@$user.service`: Enables the VNC service for
      each user.
    - `systemctl start vncserver@$user.service`: Starts the VNC service for
      each user.
1. This has to be done for each new user with `USERNAME`:
   ```
   # systemctl enable vncserver@$USERNAME.service
   # systemctl start vncserver@$USERNAME.service
   ```
1. **Client-Side Instructions**:
    - **Windows/MacOS**: Download and install the [TigerVNC viewer](https://tigervnc.org/).
    - Connect to the server using the IP and port `5901`.

### Environment Modules

1. **Install Environment Modules**:
   ```
   # dnf install environment-modules -y
   ```
1. **Set Up Modules**:
    - Create module files for MATLAB and Cadence software under `/usr/share/modules/modulefiles/`.
    - Example for MATLAB:
      ```tcl
      #%Module1.0
      proc ModulesHelp { } {
          puts stderr "MATLAB R2023a"
      }
      module-whatis "Loads MATLAB R2023a"
      prepend-path PATH /opt/matlab/R2023a/bin
      ```

### PDK

PDKs should be installed under `/opt/pdk`, where `/opt/ -> /mnt/tools/`.

#### X-Fab

...

#### Cadence

...

#### FreePDK

...

### Cadence Design Framework

1. **Download and Install Cadence Tools**:
    - Download **InstallScape** from the [Cadence Support website](https://support.cadence.com).
    - Install under `/opt/cadence/` using the InstallScape GUI.
1. **Adjust Modules**:
    - Create a module file to load the Cadence environment variables when needed.
1. **Check for missing Libraries**:
    - All Cadnce software comes with a `checkSysConf` script located under
      `tools/bin/`.
    - Modify this script to accept `AlmaLinux` as valid plattform.
    - Run the script and install missing libraries as needed.
    - You can find old packages in the [CentOS Vault](https://vault.centos.org/).

### Install EDATools

...

### MATLAB Installation

1. **Download and Install MATLAB**:
    - Download MATLAB and install it globally under `/opt/matlab/`.
    - Users will need to activate MATLAB with their own MathWorks accounts.
1. **Create Module Files**:
    - Follow similar steps as above to create a module for MATLAB, making it
      easy for users to load different versions.

## Retrieving Backed-Up User Data

1. Use `rsync` to restore user home directories:
   ```
   # rsync -avP /mnt/data/backup/home-backup/ /home/
   ```

## Setting Up SMB Share for Windows Users

### Using PAM for Samba Authentication

1. **Install Necessary Packages**:
    - Ensure you have the required Samba and PAM packages installed:
      ```
      # dnf install samba samba-client samba-common samba-winbind
      ```
1. **Configure Samba to Use PAM Authentication**:
    - Edit the Samba configuration file `/etc/samba/smb.conf` to use PAM for
      authentication. Add or modify the following settings:
      ```ini
      [global]
         workgroup = WORKGROUP
         server string = Samba Server
         security = user
         map to guest = Bad User
         dns proxy = no
      
         # Use PAM for authentication
         pam password = sandi
         passdb backend = tdbsam
      
         # Optional: Use Winbind for domain membership
         # winbind nss info = rfc2307
         # idmap config * : range = 10000-20000
         # idmap config * : backend = tdb
         # idmap config * : cache time = 3600
         # idmap config * : rangesize = 1000
         # idmap config * : backend = rid
         # idmap config * : backend = sss
         # idmap config * : backend = ad
      
      [share]
         path = /mnt/data/share
         browsable = yes
         writable = yes
         guest ok = no
         read only = no
         create mask = 0755
         directory mask = 0755
         valid users = @users
      ```
    - `pam password = sandi`: This tells Samba to use PAM for password
      management.
1. **Configure PAM for Samba**:
    - Edit the PAM configuration file for Samba `/etc/pam.d/samba`:
      ```ini
      # /etc/pam.d/samba
      auth        required      password-auth
      account     required      password-auth
      password    required      password-auth
      session     required      password-auth
      ```
    - This configuration ensures that Samba will use the standard Linux PAM
      system for authentication.
1. **Restart Samba Services**:
    - Apply the changes by restarting the Samba services:
      ```
      # systemctl restart smb.service
      # systemctl restart nmb.service
      ```
1. **Configure Samba Shares and Permissions**:
    - Ensure that your Samba shares are properly configured and permissions are
      set so that users can access them without issues. For instance:
      ```
      # chown -R root:users /mnt/data/share
      # chmod -R 2775 /mnt/data/share
      ```

## Install the ThreadRipper `man` page

1. **Clone this repository under `/opt/`**:
   ```
   $ cd /opt/
   # git clone https://gitlab-forschung.reutlingen-university.de/eda/threadwiki.git
   ```
1. **Link the service and timer into `/etc/systemd/system/`**:
   ```
   # ln -s /opt/threadwiki/service/threadwiki.service /etc/systemd/system/
   # ln -s /opt/threadwiki/service/threadwiki.timer /etc/systemd/system/
   ```
1. **Enable and start the service**:
   ```
   # systemctl enable threadwiki.timer
   # systemctl start threadwiki.timer
   ```
    - This will periodically pull the repository so that the `man` page is
      always up to date.
1. **Test the `man` page**:
   ```
   $ man threadripper
   ```

## Automating User Data Backup

1. **Create a `systemd` Service**:
    - Create `/etc/systemd/system/backup.service`:
      ```ini
      [Unit]
      Description=Backup Home Directories

      [Service]
      ExecStart=/usr/bin/rsync -avz /home/ /mnt/data/backup/home-backup/
      ```
1. **Create a Timer for Weekly Backups**:
    - Create `/etc/systemd/system/backup.timer`:
      ```ini
      [Unit]
      Description=Runs backup weekly

      [Timer]
      OnCalendar=weekly
      Persistent=true

      [Install]
      WantedBy=timers.target
      ```
    - Enable and start the timer:
      ```
      # systemctl enable --now backup.timer
      ```

## Future Updates and Upgrades

### Package Updates

1. **Regular System Updates**:
   ```
   # dnf update -y
   ```
1. **Updating NVIDIA Drivers**:
    - Use the RPM Fusion repository to install the latest version:
      ```
      # dnf update akmod-nvidia
      ```

### AlmaLinux Distribution Upgrade

1. **Check for Major Upgrades**:
   ```
   # dnf check-update
   ```
1. **Run Upgrade Command** (if necessary):
   ```
   # dnf upgrade --refresh
   ```

## Client Side Configuration

### X Forwarding 

Add the following line to local `~/.ssh/config` to avoid X forwarding timeouts:

```
Host *
    ForwardX11Timeout 7d
```
