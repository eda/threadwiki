#!/bin/sh

cd /opt/threadwiki
git pull
make install

mv /opt/threadwiki/service/threadwiki.* /etc/systemd/system/
